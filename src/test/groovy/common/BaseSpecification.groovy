package common

import pl.piotrkulma.cellularautomat.loader.CellularArrayLoader
import pl.piotrkulma.cellularautomat.loader.ResourceCellularArrayTxtLoader
import pl.piotrkulma.cellularautomat.model.CellularArray
import spock.lang.Specification

class BaseSpecification extends Specification {
    protected CellularArray loadCellularArray(String file) {
        final CellularArrayLoader loader = new ResourceCellularArrayTxtLoader()
        return loader.loadFromFile("/cellular_array00.txt")
    }
}
