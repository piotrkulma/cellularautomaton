package pl.piotrkulma.cellularautomat.evolution.neighborhood;

import common.BaseSpecification
import pl.piotrkulma.cellularautomat.model.CellularArray;
import spock.lang.Unroll;

class MooreNeighborhoodFinderTest extends BaseSpecification {
    static ALIVE = 1
    static DEAD = 0

    @Unroll
    def 'Checking closed space cell array at #indexX:#indexY position with #n neighbours'() {
        given:
        final neighborhoodFinder = new MooreNeighbourhoodFinder(new CSNeighbourhood())

        final CellularArray array = loadCellularArray("/cellular_array00.txt")

        when:
        final int neighbours = neighborhoodFinder.countNeighboursWithState(array, state, indexX, indexY)

        then:
        neighbours == n

        where:
        n | indexX | indexY | state
        0 | 10     | 0      | ALIVE
        0 | 19     | 10     | ALIVE
        0 | 10     | 19     | ALIVE
        0 | 0      | 10     | ALIVE
        1 | 0      | 0      | ALIVE
        2 | 1      | 7      | ALIVE
        3 | 2      | 3      | ALIVE
        4 | 5      | 4      | ALIVE
        5 | 9      | 4      | ALIVE
        8 | 1      | 4      | ALIVE
        0 | 1      | 4      | DEAD

    }
}
