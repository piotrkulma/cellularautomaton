package pl.piotrkulma.cellularautomat.loader

import common.BaseSpecification
import pl.piotrkulma.cellularautomat.model.CellularArray
import spock.lang.Unroll

class CellularArrayTxtLoaderTest extends BaseSpecification {
    @Unroll
    def 'Successful loaded CellularArray #indexX #indexY #value'() {
        when:
        CellularArray array = loadCellularArray("/cellular_array00.txt")

        then:
        array.getWidth() == 20
        array.getHeight() == 20

        array.getCell(indexX, indexY).isPresent() == true
        array.getCell(indexX, indexY).get().state == value

        where:
        indexX | indexY | value
        0      | 0      | 0
        1      | 1      | 1
        18     | 1      | 2
        1      | 18     | 3
        18     | 18     | 4
    }
}
