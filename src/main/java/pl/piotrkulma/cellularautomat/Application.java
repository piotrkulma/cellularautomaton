package pl.piotrkulma.cellularautomat;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Application extends javafx.application.Application {
    public static void main(final String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage stage) throws Exception {
        final FXMLLoader fxmlLoader = new FXMLLoader();
        final Parent root = fxmlLoader.load(Application.class.getClassLoader().getResourceAsStream("main_ui.fxml"));
        final Scene scene = new Scene(root, 800, 500);

        stage.setTitle("Cell evolution");
        stage.setScene(scene);
        stage.show();
    }
}
