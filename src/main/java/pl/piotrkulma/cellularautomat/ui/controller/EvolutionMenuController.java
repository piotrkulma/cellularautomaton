package pl.piotrkulma.cellularautomat.ui.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioMenuItem;
import pl.piotrkulma.cellularautomat.ApplicationConfig;
import pl.piotrkulma.cellularautomat.ui.renderer.CellularArrayRenderer;
import pl.piotrkulma.cellularautomat.ui.renderer.GOLCellularArrayRenderer;
import pl.piotrkulma.cellularautomat.ui.renderer.WireWorldCellularArrayRenderer;

import java.net.URL;
import java.util.ResourceBundle;

public class EvolutionMenuController implements Initializable {
    @FXML
    private RadioMenuItem gameOfLifeRadio;
    @FXML
    private RadioMenuItem wireWorldRadio;

    private WireWorldCellularArrayRenderer wireWorldRenderer;
    private GOLCellularArrayRenderer golRenderer;

    private CellularArrayRenderer mainCellularArrayRenderer;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        wireWorldRenderer = new WireWorldCellularArrayRenderer();
        golRenderer = new GOLCellularArrayRenderer();

        selectRenderer();
    }

    @FXML
    public void onGolMenuItemAction(final ActionEvent action) {
        setGameOfLifeEvolution();
    }

    @FXML
    public void onWireWorldMenuItemAction(final ActionEvent action) {
        setWireWorldEvolution();
    }

    public CellularArrayRenderer getEvolutionRenderer() {
        return mainCellularArrayRenderer;
    }

    private void selectRenderer() {
        if(wireWorldRadio.isSelected()) {
            setWireWorldEvolution();
        } else if(gameOfLifeRadio.isSelected()) {
            setGameOfLifeEvolution();
        }
    }

    private void setWireWorldEvolution() {
        mainCellularArrayRenderer = wireWorldRenderer;
        ApplicationConfig.setWireWorldEvolutionConfig();
    }

    private void setGameOfLifeEvolution() {
        mainCellularArrayRenderer = golRenderer;
        ApplicationConfig.setGameOfLifeEvolutionConfig();
    }
}
