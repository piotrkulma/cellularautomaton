package pl.piotrkulma.cellularautomat.ui.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import lombok.extern.slf4j.Slf4j;
import pl.piotrkulma.cellularautomat.ApplicationConfig;
import pl.piotrkulma.cellularautomat.evolution.cell.WireWorldCellEvolution;

import java.net.URL;
import java.util.ResourceBundle;

@Slf4j
public class ButtonsController implements Initializable {
    @FXML
    private Button startStopButton;
    @FXML
    private RadioButton cellStateTwo;
    @FXML
    private RadioButton cellStateThree;
    @FXML
    private RadioButton cellStateFour;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        setActiveState();
    }

    @FXML
    public void onStartStopButtonAction(final ActionEvent action) {
        if(ApplicationConfig.isEvolutionEnable()) {
            ApplicationConfig.setEvolutionEnable(false);
            startStopButton.setText("Start");
        } else {
            ApplicationConfig.setEvolutionEnable(true);
            startStopButton.setText("Stop");
        }
    }

    @FXML
    public void onRadioButtonOneAction() {
        ApplicationConfig.setActiveState(WireWorldCellEvolution.EMPTY);
    }

    @FXML
    public void onRadioButtonTwoAction() {
        ApplicationConfig.setActiveState(WireWorldCellEvolution.HEAD);
    }

    @FXML
    public void onRadioButtonThreeAction() {
        ApplicationConfig.setActiveState(WireWorldCellEvolution.TAIL);
    }

    @FXML
    public void onRadioButtonFourAction() {
        ApplicationConfig.setActiveState(WireWorldCellEvolution.CONNECTOR);
    }

    private void setActiveState() {
        if(cellStateTwo.isSelected()) {
            ApplicationConfig.setActiveState(WireWorldCellEvolution.HEAD);
        } else if(cellStateThree.isSelected()) {
            ApplicationConfig.setActiveState(WireWorldCellEvolution.TAIL);
        } else if(cellStateFour.isSelected()) {
            ApplicationConfig.setActiveState(WireWorldCellEvolution.CONNECTOR);
        }

        ApplicationConfig.setActiveState(WireWorldCellEvolution.EMPTY);
    }
}
