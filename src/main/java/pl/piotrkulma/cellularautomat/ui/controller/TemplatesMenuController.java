package pl.piotrkulma.cellularautomat.ui.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import pl.piotrkulma.cellularautomat.ApplicationConfig;
import pl.piotrkulma.cellularautomat.loader.ResourceCellularArrayTxtLoader;
import pl.piotrkulma.cellularautomat.model.CellularArray;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class TemplatesMenuController implements Initializable {
    private CellularArray template;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {

    }

    @FXML
    public void onNoneTemplateMenuItemAction(final ActionEvent action) {
        ApplicationConfig.setTemplate(Optional.empty());
    }

    @FXML
    public void onBlinker01TemplateMenuItemAction(final ActionEvent action) {
        ResourceCellularArrayTxtLoader loader = new ResourceCellularArrayTxtLoader();
        template = loader.loadFromFile("/templates/blinker01.txt");

        ApplicationConfig.setTemplate(Optional.of(template));
    }

    @FXML
    public void onGlider01TemplateMenuItemAction(final ActionEvent action) {
        ResourceCellularArrayTxtLoader loader = new ResourceCellularArrayTxtLoader();
        template = loader.loadFromFile("/templates/glider01.txt");

        ApplicationConfig.setTemplate(Optional.of(template));
    }
}
