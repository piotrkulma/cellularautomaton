package pl.piotrkulma.cellularautomat.ui.controller;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import lombok.extern.slf4j.Slf4j;
import pl.piotrkulma.cellularautomat.ApplicationConfig;
import pl.piotrkulma.cellularautomat.loader.ResourceCellularArrayTxtLoader;
import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;
import pl.piotrkulma.cellularautomat.model.FillingFragmentException;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

@Slf4j
public class CanvasController implements Initializable {
    @FXML
    private Canvas canvas;

    private boolean dragged = false;
    private CellularArray arrayCopy;
    private GraphicsContext graphicsContext;
    private Timeline animation;
    private final CellularArray array = getCellularArray();

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        graphicsContext = canvas.getGraphicsContext2D();

        initAnimation();
    }

    @FXML
    public void onCanvasMouseDragged(final MouseEvent mouseEvent) {
        dragged = true;
        setCellOnMouseEvent(mouseEvent);
        dragged = false;
    }

    @FXML
    public void onCanvasMousePressed(final MouseEvent mouseEvent) {
        if(!dragged && !isTemplateActive()) {
            setCellOnMouseEvent(mouseEvent);
        }

        if(isTemplateActive()) {
            fillPermanentWithFragment(getArrayCellPositionX(mouseEvent), getArrayCellPositionY(mouseEvent));
        }
    }

    @FXML
    public void onCanvasMouseReleased(final MouseEvent mouseEvent) {
        // evolution = true;
    }

    @FXML
    public void onCanvasMouseMoved(final MouseEvent mouseEvent) {
        synchronized (array) {
            if(isValidTemplateActive(mouseEvent)) {
                fillTemporaryWithFragment(getArrayCellPositionX(mouseEvent), getArrayCellPositionY(mouseEvent));
            }
        }
    }

    private void fillPermanentWithFragment(final int xIndex, final int yIndex) {
        fillWithFragment(array,
                ApplicationConfig.getTemplate().orElseThrow(() -> new FillingFragmentException("No active template found")),
                xIndex, yIndex);
    }

    private void fillTemporaryWithFragment(final int xIndex, final int yIndex) {
        arrayCopy = array.copy();
        fillWithFragment(arrayCopy,
                ApplicationConfig.getTemplate().orElseThrow(() -> new FillingFragmentException("No active template found")),
                xIndex, yIndex);
    }

    private void fillWithFragment(final CellularArray array, final CellularArray fragment, final int xIndex, final int yIndex) {
        try {
            ApplicationConfig.getCellularArrayHelper().fillArrayWithFragment(array, fragment, xIndex, yIndex);
        } catch (FillingFragmentException e) {
            log.error(String.format("Cannot set fragment into %s:%s", xIndex, yIndex), e);
        }
    }

    private boolean isValidTemplateActive(final MouseEvent mouseEvent) {
        return isTemplateActive() && isTemplateFitIntoCanvas(mouseEvent);
    }

    private boolean isTemplateActive() {
        return ApplicationConfig.getTemplate().isPresent();
    }

    private boolean isTemplateFitIntoCanvas(final MouseEvent mouseEvent) {
        return ApplicationConfig.getCellularArrayHelper().isValidOperation(
                array,
                ApplicationConfig.getTemplate().get(),
                getArrayCellPositionX(mouseEvent),
                getArrayCellPositionY(mouseEvent));
    }

    private void setCellOnMouseEvent(final MouseEvent mouseEvent) {
        array.setCell(Cell.builder().state(ApplicationConfig.getActiveState()).build(),
                getArrayCellPositionX(mouseEvent),
                getArrayCellPositionY(mouseEvent));
    }

    private int getArrayCellPositionX(final MouseEvent mouseEvent) {
        return (int) (mouseEvent.getX() / ApplicationConfig.getCellConfig().getCellRenderWidth());
    }

    private int getArrayCellPositionY(final MouseEvent mouseEvent) {
        return (int) (mouseEvent.getY() / ApplicationConfig.getCellConfig().getCellRenderWidth());
    }

    private void initAnimation() {
        animation = new Timeline(
                new KeyFrame(
                        Duration.seconds(0.1),
                        actionEvent -> {
                            ApplicationConfig.getRenderer()
                                    .orElseThrow(() -> new RuntimeException("No active renderer found"))
                                    .renderFrame(getArray(), graphicsContext);
                            if(!ApplicationConfig.getTemplate().isPresent() && ApplicationConfig.isEvolutionEnable()) {
                                array.fill(ApplicationConfig.getActualEvolutionConfig().getEvolutionHelper().evolveArray(array));
                            }
                        }));
        animation.setCycleCount(Animation.INDEFINITE);
        animation.play();
    }

    private CellularArray getArray() {
        if(ApplicationConfig.getTemplate().isPresent()) {
            return Optional.ofNullable(arrayCopy).orElse(array);
        }

        return array;
    }

    private CellularArray getCellularArray() {
        ResourceCellularArrayTxtLoader loader = new ResourceCellularArrayTxtLoader();
        return loader.loadFromFile("/cellular_array00.txt");
    }
}
