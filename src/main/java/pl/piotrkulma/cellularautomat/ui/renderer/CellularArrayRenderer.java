package pl.piotrkulma.cellularautomat.ui.renderer;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.NoArgsConstructor;
import pl.piotrkulma.cellularautomat.ApplicationConfig;
import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;

@NoArgsConstructor
public abstract class CellularArrayRenderer {
    public void renderFrame(final CellularArray array, final GraphicsContext graphicsContext) {
        final int cellWidth = ApplicationConfig.getCellConfig().getCellRenderWidth();
        final int cellHeight = ApplicationConfig.getCellConfig().getCellRenderHeight();
        for(int i=0; i<array.getWidth(); i++) {
            for(int j=0; j<array.getHeight(); j++) {
                Cell cell = array.getCell(i, j).get();
                renderCell(graphicsContext, cell, cellWidth, cellHeight, i, j);

                graphicsContext.setFill(Color.BLACK);
                graphicsContext.strokeRect(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
            }
        }
    }

    protected abstract void renderCell(
            final GraphicsContext graphicsContext,
            final Cell cell,
            final int cellWidth,
            final int cellHeight,
            final int i,
            final int j);
}
