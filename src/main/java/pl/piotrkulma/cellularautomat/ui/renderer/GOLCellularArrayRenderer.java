package pl.piotrkulma.cellularautomat.ui.renderer;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import pl.piotrkulma.cellularautomat.evolution.cell.GOLCellEvolution;
import pl.piotrkulma.cellularautomat.model.Cell;

public class GOLCellularArrayRenderer extends CellularArrayRenderer {
    @Override
    protected void renderCell(
            final GraphicsContext graphicsContext,
            final Cell cell, final int cellWidth,
            final int cellHeight,
            final int i, final int j) {
        if(cell.getState() == GOLCellEvolution.ALIVE_STATE) {
            graphicsContext.setFill(Color.BLUE);
            graphicsContext.fillRect(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
        } else {
            graphicsContext.setFill(Color.WHITE);
            graphicsContext.fillRect(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
        }
    }
}
