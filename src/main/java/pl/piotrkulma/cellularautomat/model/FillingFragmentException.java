package pl.piotrkulma.cellularautomat.model;

public class FillingFragmentException extends RuntimeException {
    public FillingFragmentException(String message) {
        super(message);
    }
}
