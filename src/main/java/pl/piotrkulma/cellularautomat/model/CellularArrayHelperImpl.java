package pl.piotrkulma.cellularautomat.model;

public class CellularArrayHelperImpl implements CellularArrayHelper {
    public CellularArray fillArrayWithFragment (
            final CellularArray mainArray, final CellularArray fragmentToFill,
            final int startAtX, final int startAtY) throws FillingFragmentException {

        if(!isValidOperation(mainArray, fragmentToFill, startAtX, startAtY)) {
            return mainArray;
        }

        for(int i=0; i<fragmentToFill.getWidth(); i++) {
            for(int j=0; j<fragmentToFill.getHeight(); j++) {
                mainArray.setCell(
                        fragmentToFill.getCell(i, j).orElseThrow(() -> new FillingFragmentException(
                                "Cell in fragment array does not exists")),
                        startAtX + i, startAtY + j);
            }
        }

        return mainArray;
    }

    public boolean isValidOperation(final CellularArray mainArray, final CellularArray fragmentToFill,
                          final int startAtX, final int startAtY) {
        return mainArray.isCellExistsAtPosition(
                startAtX + fragmentToFill.getWidth() - 1,
                startAtY + fragmentToFill.getHeight() - 1);
    }

}
