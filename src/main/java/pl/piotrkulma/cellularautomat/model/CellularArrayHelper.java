package pl.piotrkulma.cellularautomat.model;

public interface CellularArrayHelper {
    CellularArray fillArrayWithFragment(
            final CellularArray mainArray, final CellularArray fragmentToFill,
            final int startAtX, final int startAtY) throws FillingFragmentException;

    boolean isValidOperation(final CellularArray mainArray, final CellularArray fragmentToFill,
                                    final int startAtX, final int startAtY);
}
