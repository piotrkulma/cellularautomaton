package pl.piotrkulma.cellularautomat.model;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder(toBuilder=true)
@ToString
public class Cell {
    private int state;
}
