package pl.piotrkulma.cellularautomat.model;

import lombok.Getter;

import java.util.Optional;

public class CellularArray {
    @Getter
    private int width;
    @Getter
    private int height;
    private Cell[][] representation;

    public CellularArray(final int width, int height) {
        this.width = width;
        this.height = height;
        this.representation = new Cell[width][height];
    }

    public synchronized void setCell(final Cell cell, final int i, final int j) {
        representation[i][j] = cell.toBuilder().build();
    }

    public Optional<Cell> getCell(final int i, final int j) {
        if(i<0 || i>=width || j<0 || j>=height) {
            return Optional.empty();
        }

        return Optional.of(representation[i][j].toBuilder().build());
    }

    public boolean isCellExistsAtPosition(final int i, final int j) {
        return i>=0 && i<width && j>=0 && j<height;
    }

    public synchronized void fill(CellularArray cellularArray) {
        this.width = cellularArray.getWidth();
        this.height = cellularArray.getHeight();

        for(int i=0; i<width; i++) {
            if (height >= 0) System.arraycopy(cellularArray.representation[i], 0, this.representation[i], 0, height);
        }
    }

    public synchronized CellularArray copy() {
        CellularArray cellularArray = new CellularArray(width, height);

        for(int i=0; i<width; i++) {
            for(int j=0; j<height; j++) {
                cellularArray.setCell(representation[i][j], i, j);
            }
        }

        return cellularArray;
    }
}
