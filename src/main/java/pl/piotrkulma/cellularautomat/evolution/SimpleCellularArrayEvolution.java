package pl.piotrkulma.cellularautomat.evolution;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.piotrkulma.cellularautomat.evolution.cell.CellEvolution;
import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;

@Slf4j
@AllArgsConstructor
public class SimpleCellularArrayEvolution implements CellularArrayEvolution {
    private CellEvolution cellEvolution;

    public CellularArray evolveArray(final CellularArray actualArray) {
        final CellularArray evolvedArray = new CellularArray(actualArray.getWidth(), actualArray.getHeight());
        for(int i=0; i<actualArray.getWidth(); i++) {
            for(int j=0; j<actualArray.getHeight(); j++) {
                final Cell evolvedCell = cellEvolution.evolveCell(actualArray, i,j);
                evolvedArray.setCell(evolvedCell, i, j);
            }
        }

        return evolvedArray;
    }
}
