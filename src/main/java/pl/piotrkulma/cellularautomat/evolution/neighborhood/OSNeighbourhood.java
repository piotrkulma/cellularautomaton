package pl.piotrkulma.cellularautomat.evolution.neighborhood;

import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;

import java.util.Optional;

/**
 * Open Space Neighborhood
 */
public class OSNeighbourhood implements Neighbourhood {
    @Override
    public Optional<Cell> getNeighbourhood(final CellularArray array, final int indexX, final int indexY) {
        if(array.getCell(indexX, indexY).isPresent()) {
            return array.getCell(indexX, indexY);
        }
        return Optional.of(findExtendedNeighbour(array, indexX, indexY));
    }

    private Cell findExtendedNeighbour(final CellularArray array, final int indexX, final int indexY) {
        int newIndexX = indexX;
        int newIndexY = indexY;

        if(indexX < 0) {
            newIndexX = array.getWidth() - 1;
        }

        if(indexX >= array.getWidth()) {
            newIndexX = 0;
        }

        if(indexY < 0) {
            newIndexY = array.getHeight() - 1;
        }

        if(indexY >= array.getHeight()) {
            newIndexY = 0;
        }

        return array.getCell(newIndexX, newIndexY)
                .orElseThrow(() -> new RuntimeException("Index value is bad calculated"));
    }
}
