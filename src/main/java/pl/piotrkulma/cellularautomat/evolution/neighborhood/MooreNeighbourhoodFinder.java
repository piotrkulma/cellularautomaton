package pl.piotrkulma.cellularautomat.evolution.neighborhood;

import lombok.AllArgsConstructor;
import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;

import java.util.function.Consumer;

@AllArgsConstructor
public class MooreNeighbourhoodFinder implements NeighbourhoodFinder {
    private Neighbourhood neighbourhood;

    public int countNeighboursWithState(final CellularArray array, final int state, final int indexX, final int indexY) {
        final Counter counter = new Counter(0);

        final Consumer<Cell> cellConsumer = c -> {
            if (c.getState() == state) {
                counter.inc();
            }
        };

        neighbourhood.getNeighbourhood(array, indexX - 1, indexY - 1).ifPresent(cellConsumer);
        neighbourhood.getNeighbourhood(array, indexX - 1, indexY).ifPresent(cellConsumer);
        neighbourhood.getNeighbourhood(array, indexX - 1, indexY + 1).ifPresent(cellConsumer);
        neighbourhood.getNeighbourhood(array, indexX, indexY - 1).ifPresent(cellConsumer);
        neighbourhood.getNeighbourhood(array, indexX, indexY + 1).ifPresent(cellConsumer);
        neighbourhood.getNeighbourhood(array, indexX + 1, indexY - 1).ifPresent(cellConsumer);
        neighbourhood.getNeighbourhood(array, indexX + 1, indexY).ifPresent(cellConsumer);
        neighbourhood.getNeighbourhood(array, indexX + 1, indexY + 1).ifPresent(cellConsumer);

        return counter.getValue();
    }

    private static class Counter {
        private int value;

        public Counter(int value) {
            this.value = value;
        }

        public void inc() {
            this.value++;
        }

        public int getValue() {
            return value;
        }
    }
}
