package pl.piotrkulma.cellularautomat.evolution.neighborhood;

import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;

import java.util.Optional;

/**
 * Closed Space Neighborhood
 */
public class CSNeighbourhood implements Neighbourhood{
    @Override
    public Optional<Cell> getNeighbourhood(final CellularArray array, final int indexX, final int indexY) {
        return array.getCell(indexX, indexY);
    }
}
