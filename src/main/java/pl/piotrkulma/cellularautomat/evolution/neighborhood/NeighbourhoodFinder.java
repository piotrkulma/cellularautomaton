package pl.piotrkulma.cellularautomat.evolution.neighborhood;

import pl.piotrkulma.cellularautomat.model.CellularArray;

public interface NeighbourhoodFinder {
    int countNeighboursWithState(final CellularArray array, final int state, final int indexX, final int indexY);
}
