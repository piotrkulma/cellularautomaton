package pl.piotrkulma.cellularautomat.evolution.neighborhood;

import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;

import java.util.Optional;

public interface Neighbourhood {
    Optional<Cell> getNeighbourhood(final CellularArray array, final int indexX, final int indexY);
}
