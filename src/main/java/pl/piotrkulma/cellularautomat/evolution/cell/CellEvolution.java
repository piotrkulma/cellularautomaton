package pl.piotrkulma.cellularautomat.evolution.cell;

import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;

public interface CellEvolution {
    Cell evolveCell(final CellularArray cellularArray, final int indexX, final int indexY);
}
