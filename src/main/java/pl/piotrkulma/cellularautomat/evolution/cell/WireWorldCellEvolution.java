package pl.piotrkulma.cellularautomat.evolution.cell;

import lombok.AllArgsConstructor;
import pl.piotrkulma.cellularautomat.evolution.neighborhood.NeighbourhoodFinder;
import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;

@AllArgsConstructor
public class WireWorldCellEvolution implements CellEvolution {
    public final static int EMPTY = 0;
    public final static int HEAD = 1;
    public final static int TAIL = 2;
    public final static int CONNECTOR = 3;

    private NeighbourhoodFinder finder;

    @Override
    public Cell evolveCell(final CellularArray cellularArray, final int indexX, final int indexY) {
        final Cell cell = cellularArray.getCell(indexX, indexY).get();

        if(cell.getState() == EMPTY) {
            return cell.toBuilder().state(EMPTY).build();
        } else if(cell.getState() == HEAD) {
            return cell.toBuilder().state(TAIL).build();
        } else if(cell.getState() == TAIL) {
            return cell.toBuilder().state(CONNECTOR).build();
        } else if(cell.getState() == CONNECTOR) {
            return evolveConnectorCell(cellularArray, cell, indexX, indexY);
        }

        return cell.toBuilder().build();
    }

    private Cell evolveConnectorCell(final CellularArray cellularArray, final Cell cell, final int indexX, final int indexY) {
        int heads = finder.countNeighboursWithState(cellularArray, HEAD, indexX, indexY);

        if(heads == 1 || heads == 2) {
            return cell.toBuilder().state(HEAD).build();
        }

        return cell.toBuilder().state(CONNECTOR).build();
    }
}
