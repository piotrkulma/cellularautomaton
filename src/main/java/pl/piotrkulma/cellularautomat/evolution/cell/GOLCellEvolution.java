package pl.piotrkulma.cellularautomat.evolution.cell;

import lombok.AllArgsConstructor;
import pl.piotrkulma.cellularautomat.evolution.neighborhood.NeighbourhoodFinder;
import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;

@AllArgsConstructor
public class GOLCellEvolution implements CellEvolution {
    public final static int DEAD_STATE = 0;
    public final static int ALIVE_STATE = 1;

    private NeighbourhoodFinder finder;

    @Override
    public Cell evolveCell(final CellularArray cellularArray, final int indexX, final int indexY) {
        final Cell cell = cellularArray.getCell(indexX, indexY).get();
        final int neighbours = finder.countNeighboursWithState(cellularArray, ALIVE_STATE, indexX, indexY);

        if (cell.getState() == DEAD_STATE && neighbours == 3) {
            return cell.toBuilder().state(ALIVE_STATE).build();
        } else if(cell.getState() == ALIVE_STATE && (neighbours < 2 || neighbours > 3)) {
            return cell.toBuilder().state(DEAD_STATE).build();
        }

        return cell.toBuilder().build();
    }
}
