package pl.piotrkulma.cellularautomat.evolution;

import pl.piotrkulma.cellularautomat.model.CellularArray;

public interface CellularArrayEvolution {
    CellularArray evolveArray(final CellularArray cellularArray);
}
