package pl.piotrkulma.cellularautomat.evolution.helper;

import pl.piotrkulma.cellularautomat.model.CellularArray;

public interface CellularArrayEvolutionHelper {
    CellularArray evolveArray(final CellularArray cellularArray);
}
