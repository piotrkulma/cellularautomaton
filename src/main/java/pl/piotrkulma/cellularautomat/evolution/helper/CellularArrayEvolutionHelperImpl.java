package pl.piotrkulma.cellularautomat.evolution.helper;

import lombok.AllArgsConstructor;
import pl.piotrkulma.cellularautomat.evolution.SimpleCellularArrayEvolution;
import pl.piotrkulma.cellularautomat.model.CellularArray;

@AllArgsConstructor
public class CellularArrayEvolutionHelperImpl implements CellularArrayEvolutionHelper {
    private SimpleCellularArrayEvolution evolution;

    public CellularArray evolveArray(final CellularArray cellularArray) {
        return evolution.evolveArray(cellularArray);
    }
}
