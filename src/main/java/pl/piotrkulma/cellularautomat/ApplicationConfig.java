package pl.piotrkulma.cellularautomat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.piotrkulma.cellularautomat.model.CellularArray;
import pl.piotrkulma.cellularautomat.model.CellularArrayHelper;
import pl.piotrkulma.cellularautomat.model.CellularArrayHelperImpl;
import pl.piotrkulma.cellularautomat.ui.renderer.CellularArrayRenderer;
import pl.piotrkulma.cellularautomat.ui.renderer.GOLCellularArrayRenderer;
import pl.piotrkulma.cellularautomat.ui.renderer.WireWorldCellularArrayRenderer;

import java.util.Optional;

public final class ApplicationConfig {
    @Getter
    @Setter
    private static boolean evolutionEnable = false;

    @Getter
    @Setter
    private static int activeState;

    @Setter
    @Getter
    private static Optional<CellularArrayRenderer> renderer = Optional.empty();

    @Setter
    @Getter
    private static Optional<CellularArray> template = Optional.empty();

    private static EvolutionConfig ACTUAL_EVOLUTION_CONFIG;

    private static CellConfig cellConfig = new CellConfig(8, 8);
    private static CellularArrayHelper cellularArrayHelper = new CellularArrayHelperImpl();

    public static EvolutionConfig getActualEvolutionConfig() {
        Optional.ofNullable(ACTUAL_EVOLUTION_CONFIG).orElseThrow(RuntimeException::new);
        return ACTUAL_EVOLUTION_CONFIG;
    }

    public static CellConfig getCellConfig() {
        return cellConfig;
    }

    public static void setWireWorldEvolutionConfig() {
        renderer = Optional.of(new WireWorldCellularArrayRenderer());
        ACTUAL_EVOLUTION_CONFIG = EvolutionConfig.getWireWorldEvolutionConfig();
    }

    public static void setGameOfLifeEvolutionConfig() {
        renderer = Optional.of(new GOLCellularArrayRenderer());
        ACTUAL_EVOLUTION_CONFIG = EvolutionConfig.getGameOfLifeEvolutionConfig();
    }

    public static CellularArrayHelper getCellularArrayHelper() {
        return cellularArrayHelper;
    }

    @AllArgsConstructor
    public static class CellConfig {
        @Getter
        private int cellRenderWidth;
        @Getter
        private int cellRenderHeight;
    }
}
