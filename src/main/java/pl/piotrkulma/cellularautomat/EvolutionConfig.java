package pl.piotrkulma.cellularautomat;

import lombok.Getter;
import pl.piotrkulma.cellularautomat.evolution.SimpleCellularArrayEvolution;
import pl.piotrkulma.cellularautomat.evolution.cell.CellEvolution;
import pl.piotrkulma.cellularautomat.evolution.cell.GOLCellEvolution;
import pl.piotrkulma.cellularautomat.evolution.cell.WireWorldCellEvolution;
import pl.piotrkulma.cellularautomat.evolution.helper.CellularArrayEvolutionHelper;
import pl.piotrkulma.cellularautomat.evolution.helper.CellularArrayEvolutionHelperImpl;
import pl.piotrkulma.cellularautomat.evolution.neighborhood.MooreNeighbourhoodFinder;
import pl.piotrkulma.cellularautomat.evolution.neighborhood.Neighbourhood;
import pl.piotrkulma.cellularautomat.evolution.neighborhood.NeighbourhoodFinder;
import pl.piotrkulma.cellularautomat.evolution.neighborhood.OSNeighbourhood;

@Getter
public class EvolutionConfig {
    private CellEvolution cellEvolution;
    private SimpleCellularArrayEvolution cellularArrayEvolution;
    private Neighbourhood neighbourhood;
    private NeighbourhoodFinder neighbourhoodFinder;
    private CellularArrayEvolutionHelper evolutionHelper;

    private EvolutionConfig() {
    }

    public static EvolutionConfig getWireWorldEvolutionConfig() {
        EvolutionConfig config = getBaseEvolutionConfig();
        config.cellEvolution =new  WireWorldCellEvolution(config.neighbourhoodFinder);
        config.cellularArrayEvolution = new SimpleCellularArrayEvolution(config.cellEvolution);
        config.evolutionHelper = new CellularArrayEvolutionHelperImpl(config.cellularArrayEvolution);
        return config;
    }

    public static EvolutionConfig getGameOfLifeEvolutionConfig() {
        EvolutionConfig config = getBaseEvolutionConfig();
        config.cellEvolution = new GOLCellEvolution(config.neighbourhoodFinder);
        config.cellularArrayEvolution = new SimpleCellularArrayEvolution(config.cellEvolution);
        config.evolutionHelper = new CellularArrayEvolutionHelperImpl(config.cellularArrayEvolution);
        return config;
    }

    private static EvolutionConfig getBaseEvolutionConfig() {
        EvolutionConfig config = new EvolutionConfig();
        config.neighbourhood = new OSNeighbourhood();
        config.neighbourhoodFinder = new MooreNeighbourhoodFinder(config.neighbourhood);

        return config;
    }
}
