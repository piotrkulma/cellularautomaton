package pl.piotrkulma.cellularautomat.loader;

import pl.piotrkulma.cellularautomat.model.CellularArray;

public interface CellularArrayLoader {
    CellularArray loadFromFile(final String path);
}
