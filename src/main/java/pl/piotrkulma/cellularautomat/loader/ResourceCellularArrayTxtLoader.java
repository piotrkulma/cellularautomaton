package pl.piotrkulma.cellularautomat.loader;

import pl.piotrkulma.cellularautomat.model.CellularArray;

import java.io.File;

public class ResourceCellularArrayTxtLoader extends CellularArrayTxtLoader {
    @Override
    public CellularArray loadFromFile(final String path) {
        return super.loadFromFile(getResourcePath(path));
    }

    private String getResourcePath(final String fileName) {
        return new File(ResourceCellularArrayTxtLoader.class.getResource(fileName).getPath()).getPath();
    }
}
