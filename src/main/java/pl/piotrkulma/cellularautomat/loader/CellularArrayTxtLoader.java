package pl.piotrkulma.cellularautomat.loader;

import lombok.NoArgsConstructor;
import pl.piotrkulma.cellularautomat.model.Cell;
import pl.piotrkulma.cellularautomat.model.CellularArray;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public class CellularArrayTxtLoader implements CellularArrayLoader {
    public CellularArray loadFromFile(final String path) {
        final List<String> data = loadData(path);
        validate(data);

        return processData(data);
    }

    private List<String> loadData(final String path) {
        try {
            return Files.lines(Paths.get(path)).collect(Collectors.toList());
        } catch(Exception e) {
            throw new RuntimeException(String.format("Cannot read from path %s", path), e);
        }
    }

    private void validate(final List<String> lines) {
        if(!isDataValid(lines)) {
            throw new RuntimeException("Array size must be N x N where N is integer greater than 0");
        }
    }

    private CellularArray processData(final List<String> lines) {
        final int width = Long.valueOf(lines.get(0).length()).intValue();
        final int height = Long.valueOf(lines.size()).intValue();

        final List<Cell> cells = lines.stream().map(line -> line.split(""))
                .flatMap(Arrays::stream)
                .map(str -> Cell.builder().state(Integer.parseInt(str)).build())
                .collect(Collectors.toList());

        return convertCellsToCellularArray(cells, width, height);
    }

    private CellularArray convertCellsToCellularArray(final List<Cell> cells, final int width, final int height) {
        int i=0;
        int j=0;

        final CellularArray model = new CellularArray(width, height);

        for(Cell cell : cells) {
            if(i == width) {
                i = 0;
                j++;
            }

            model.setCell(cell, i++, j);
        }

        return model;
    }

    private boolean isDataValid(final List<String> lines) {
        final int firstElementSize =
                lines.stream().findFirst().orElseThrow(() -> new RuntimeException("Row has zero length")).length();
        return lines.size() > 0 &&
                lines.stream().allMatch(line -> line.length() == firstElementSize);
    }
}
